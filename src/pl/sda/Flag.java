package pl.sda;

import java.util.Scanner;

public class Flag {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final Integer w = Integer.valueOf(scanner.nextLine());
        for (int wi = 0; wi < w; wi++) {
            for (int hi = 0; hi < w; hi++) {
                if (wi >= w / 2) {
                    if (hi >= w / 2) {
                        System.out.print("o");
                    } else {
                        System.out.print("x");
                    }
                } else {
                    if (hi >= w / 2) {
                        System.out.print("x");
                    } else {
                        System.out.print("o");
                    }
                }
            }
            System.out.println();
        }
    }
}
