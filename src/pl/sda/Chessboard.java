package pl.sda;

import java.util.Scanner;

public class Chessboard {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        final Integer w = Integer.valueOf(scanner.nextLine());
        final Integer h = Integer.valueOf(scanner.nextLine());

        for (int wi = 0; wi < w; wi++) {
            for (int hi = 0; hi < h; hi++) {
                if (wi % 2 == 0) {
                    if (hi % 2 != 0) {
                        System.out.print("o");
                    } else {
                        System.out.print("x");
                    }
                } else {
                    if (hi % 2 != 0) {
                        System.out.print("x");
                    } else {
                        System.out.print("o");
                    }
                }
            }
            System.out.println();
        }
    }
}
