package pl.sda.list;

public class ListMain {
    public static void main(String[] args) {
        StringListElement element1 = new StringListElement();
        element1.setValue("Ala");
        StringListElement element2 = new StringListElement();
        element2.setValue("ma");
        StringListElement element3 = new StringListElement();
        element3.setValue("kota");

        element1.setNext(element2);
        element2.setNext(element3);

        final StringListElement nextOne = element1.next();
        System.out.println(nextOne.getValue());

        final StringListElement nextTwo = nextOne.next();
        System.out.println(nextTwo.getValue());

        System.out.println("NExt next");
        System.out.println(element1.next().next().getValue());

    }
}
