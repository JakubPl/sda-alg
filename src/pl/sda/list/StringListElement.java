package pl.sda.list;

public class StringListElement {
    private String value;
    private StringListElement nextElement;

    public StringListElement next() {
        return nextElement;
    }

    public void setNext(StringListElement element) {
        this.nextElement = element;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
