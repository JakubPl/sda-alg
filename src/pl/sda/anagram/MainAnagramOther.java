package pl.sda.anagram;

import java.util.Arrays;
import java.util.Scanner;

public class MainAnagramOther {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        String anagramCandidate1 = scanner.nextLine();
        String anagramCandidate2 = scanner.nextLine();

        //jesli jest taka sama to:
        //zamien litery w obu ciagach na male i zlikwiduj spacje
        anagramCandidate1 = anagramCandidate1.replace(" ", "").toLowerCase();
        anagramCandidate2 = anagramCandidate2.replace(" ", "").toLowerCase();

        final char[] charArray1 = anagramCandidate1.toCharArray();
        final char[] charArray2 = anagramCandidate2.toCharArray();

        Arrays.sort(charArray1);
        Arrays.sort(charArray2);

        final boolean isAnagram = Arrays.equals(charArray1, charArray2);
        if(isAnagram) {
            System.out.println("Jest");
        } else {
            System.out.println("Nie jest");
        }
    }
}
