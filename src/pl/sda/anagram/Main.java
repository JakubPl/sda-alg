package pl.sda.anagram;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        String anagramCandidate1 = scanner.nextLine();
        String anagramCandidate2 = scanner.nextLine();


        //jesli dlugosc anagramCandidate1 nie jest taka sama jaka anagramCandidate2 to:
        //zakoncz program - falsz
        if(anagramCandidate1.length() != anagramCandidate2.length()) {
            System.out.println("Nie");
            return;
        }
        //jesli jest taka sama to:
        //zamien litery w obu ciagach na male i zlikwiduj spacje
        anagramCandidate1 = anagramCandidate1.replace(" ", "").toLowerCase();
        anagramCandidate2 = anagramCandidate2.replace(" ", "").toLowerCase();

        final char[] charsFromAnagramCandidate1 = anagramCandidate1.toCharArray();
        for (char ch: charsFromAnagramCandidate1){
            //sprawdz czy ch znajduje sie w anagramCandidate2
            boolean isInAnagramCandidate2 = anagramCandidate2.contains(ch+"");
            //jesli isInAnagramCandidate2 nie jest prawda to:
            //zakoncz program - falsz
            if(!isInAnagramCandidate2) {
                System.out.println("Nie");
                return;
            }
            //jesli jest prawda to:
            int index = anagramCandidate2.indexOf(ch);//na ktorym indeksie anagramCandidate2 zawiera ta litere
            //usun litere z tego indeksu (to akurat opisze nie pseudokodem, bo nie jest oczywiste)
            anagramCandidate2 = anagramCandidate2.substring(0, index) + anagramCandidate2.substring(index + 1);
        }
        System.out.println("Jest anagramem");
    }
}
