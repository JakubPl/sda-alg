package pl.sda.task4;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Human human0 = new Human("Adam", 15, Gender.MALE);
        Human human1 = new Human("Paweł", 20, Gender.MALE);
        Human human2 = new Human("Basia", 30, Gender.FEMALE);
        Human human3 = new Human("Kasia", 25, Gender.FEMALE);
        Human human4 = new Human("Marcin", 101, Gender.MALE);

        List<Human> humans = new LinkedList<>();
        humans.add(human0);
        humans.add(human1);
        humans.add(human2);
        humans.add(human3);
        humans.add(human4);

       /* final Stream<Human> stream = humans.stream();
        final Stream<Human> maleStream = stream.filter(h -> h.getGender() == Gender.FEMALE);
        maleStream.forEach(person -> System.out.println(person.getName()));*/

        /*for (Human person : maleList) {
            System.out.println(person.getName());
        }*/

        final Stream<Human> stream = humans.stream();
        final Map<Boolean, List<Human>> partitionedGenders = stream.collect(
                Collectors.partitioningBy(e -> e.getGender() == Gender.FEMALE)
        );

        partitionedGenders.get(false).forEach(e -> System.out.println(e.getName()));

        System.out.println("Ludzie powyzej 18");
        humans.stream()
                .filter(person -> person.getAge() >= 18)
                .forEach(e -> System.out.println(e.getName()));

        System.out.println("Wielokrotnosc 10:");
        humans.stream()
                .filter(person -> person.getAge() % 10 == 0)
                .forEach(e -> System.out.println(e.getName()));
        System.out.println("Suma lat w liscie:");

        /*final Stream<Integer> integerStream = humans.stream()
                .map(Human::getAge);*/

        final IntStream ageIntStream = humans.stream()
                .mapToInt(Human::getAge);

        System.out.println(ageIntStream.sum());

        System.out.println("Wszystkie osoby majace si w imieniu");
        humans.stream()
                .filter(person -> person.getName().contains("si"))
                .forEach(person -> System.out.println(person.getName()));

    }
}
