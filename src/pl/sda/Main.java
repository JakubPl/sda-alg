package pl.sda;

import java.util.Comparator;
import java.util.PriorityQueue;

public class Main {
    public static void main(String[] args) {

        PriorityQueue<String> a = new PriorityQueue<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });

        a.add("a");
        a.add("b");
        a.add("a");
        a.add("z");
        a.add("b");

        System.out.println(a.poll());
        System.out.println(a.poll());
        System.out.println(a.poll());
        System.out.println(a.poll());
        System.out.println(a.poll());

    }

    // Sortowanie bąbelkowe (https://pl.wikipedia.org/wiki/Sortowanie_b%C4%85belkowe):
    private static void bubbleSort(int[] array) {
        // Pojedynczy przebieg poniższej pętli reprezentuje przejście jednego bąbelka.
        for (int i = 0; i < array.length - 1; i++) {
            // Przebieg poniższej pętli reprezentuje porównanie sąsiednich elementów (krok bąbelka w "górę").
            for (int j = 0; j < array.length - 1 - i; j++) {
                // Jeśli kolejność sąsiadujących elementów się nie zgadza
                if (array[j] > array[j + 1]) {
                    // to zamieniamy je miejscami.
                    swapElements(array, j, j + 1);
                }
            }
        }
    }

    private static void swapElements(int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

    // https://pl.wikipedia.org/wiki/Sortowanie_przez_wybieranie
    private static void selectionSort(int[] array) {
        // Pętla zewnętrzna będzie reprezentowała całe sortowanie.
        // Pojedynczy przebieg będzie reprezentował wyszukanie najmniejszego elementu
        // w części nieposortowanej i zamianę go z pierwszym elementem w części nieposortowanej.
        // Pętla przebiegnie liczba elementów tablicy - 1 razy.
        for (int i = 0; i < array.length - 1; i++) {
            // Wewnątrz tej pętli będzie pętla służąca do znalezienia indeksu elementu najmniejszego
            // w części nieposortowanej. Pętla przebiegnie liczba elementów tablicy razy minus liczba elementów
            // posortowanej części.
            // i to indeks pierwszego elementu nieposortowanej części. Na starcie założymy, że ten pierwszy
            // element ma najmniejszą wartość.
            int minIndex = i;
            // Następnie przebiegniemy przez wszystkie następne
            for (int j = i + 1; j < array.length; j++) {
                // I jeśli trafimy na element o mniejszej wartości
                if (array[j] < array[minIndex]) {
                    // To zaktualizujemy wartość indeksu elementu o najmniejszej wartości z części nieposortowanej.
                    minIndex = j;
                }
            }
            // Po znalezieniu tego indeksu zamieniamy wartościami element pierwszy
            // z części nieposortowanej z elementem o znalezionym indeksie.
            swapElements(array, minIndex, i);
        }
    }

    // https://pl.wikipedia.org/wiki/Sortowanie_przez_wstawianie
    private static void insertionSort(int[] array) {
        // Mamy pętlę, której jeden przebieg reprezentuje wstawienie elementu do posortowanej części.
        // Licznik tej pętli to początkowy (bo będziemy go przesuwali) indeks wstawianego elementu.
        for (int i = 1; i < array.length; i++) {
            // Wstawienie elementu wygląda następująco: póki mamy element po lewej stronie
            // (póki indeks jest większy od zera) i póki element z lewej strony jest większy -
            // zamieniamy nasz element z elementem z jego lewej strony.
            // j to indeks wstawianego elementu. Na starcie jest równy i, ale gdy go będziemy wstawiać
            // (przesuwać w lewo), to jego indeks będzie malał. Działamy póki mamy element z lewej strony
            // (j > 0) i póki z lewej strony mamy element większy (array[j - 1] > array[j]).
            // Po zamianie z elementem po lewej stronie zmniejszamy indeks o 1 (bo przesuneliśmy wstawiany
            // element o jedno miejsce w lewo).
            for (int j = i; j > 0 && array[j - 1] > array[j]; j--) {
                swapElements(array, j, j - 1);
            }
        }
    }
}
