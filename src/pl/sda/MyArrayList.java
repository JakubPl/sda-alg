package pl.sda;

import java.util.Arrays;

public class MyArrayList {
    private String[] elements;
    private int currentLast;

    public MyArrayList() {
        this.elements = new String[10];
    }

    public void add(String element) {
        if(currentLast + 1 > element.length()) {
            elements = Arrays.copyOf(elements, elements.length * 2);
        }
        elements[currentLast++] = element;
    }

    public void remove(int i) {
        int lengthToCopy = currentLast - i - 1;
        if (lengthToCopy > 0) {
            System.arraycopy(elements, i + 1, elements, i, lengthToCopy);
        }
    }

    public String get(int i) {
        return elements[i];
    }
}
